# Data Dictionary schemas

**WIP!** This repo is under active development. Schemas are not yet ready for production use. See [milestones](https://gitlab.com/bim-alliance/data-lexikon/data-dictionary-schemas/-/milestones) for more info.

This repository (repo) contains JSON schemas to handle data exchange between data dictionaries. Entities that are subject of exchange is e.g. data templates, EPD:s and properties. This is achieved by implementing the standards [SS-EN ISO 12006-3:2022](https://www.sis.se/produkter/informationsteknik-kontorsutrustning/ittillampningar/ittillampningar-inom-bygg-och-anlaggningsindustri/ss-en-iso-12006-32022/), [SS-EN ISO 23387:2020](https://www.sis.se/produkter/informationsteknik-kontorsutrustning/ittillampningar/ittillampningar-inom-bygg-och-anlaggningsindustri/ss-en-iso-233872020/) (next version) and [SS-EN ISO 22057:2022](https://www.sis.se/produkter/informationsteknik-kontorsutrustning/ittillampningar/ittillampningar-inom-bygg-och-anlaggningsindustri/ss-en-iso-220572022/).

## Docs

* [Flowcharts](./docs/flowcharts.md)

## Repo structure

### Naming conventions

In this repo files should be named accordingly:

* JSON schema files shall be named: `<schema name>.<schema version number>.schema.json`
* Data files shall be named `<uuid>.<schema name>.<schema version number>.json`

### Folder structure

* Schemas are stored in [schemas](./schemas/)
* Datasets are stored in subfolders in [datasets](./datasets/)
    * E.g. [Basic dataset](./datasets/basic_example/)

## Strategies

* Abstract entities are defined as type "object", and
* All other entities are defined as "allOf" with references to all above supertypes. The entity specific attributes are defined in `$defs`
* When express schema states a relation between entities, the reference should be saved as a "ReferenceUuid" property according to the [`entityRef schema`](./schemas/entityref.1.schema.json).
* `xtdRational`, `xtdMultilevelText` and `xtdText`, `xtdRelationshipKindEnum` are not considered as "stand alone entities" when they are used as subentities. The values for these entities are therefore always embedded on a "parent entity". Because of this there is no real reason for providing them with unique ID:s. We are doing this just to follow the standard.

## Example of validation

AJV is a tool for validating both schemas and data. It can be installed by:

```
npm install -g ajv-cli ajv-keywords ajv-formats
```

**Validate dictionaries**

This command validates all entities which has a file suffix of "xtddictionary.1.json". Copy and paste in into your terminal to test

```
ajv validate --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtddictionary.1.schema.json \
-r "schemas/!(xtddictionary).1.schema.json" \
-d "datasets/basic_example/*.xtddictionary.1.json"
```

**Validate properties**

```
ajv validate --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdproperty.1.schema.json \
-r "schemas/!(xtdproperty).1.schema.json" \
-d "datasets/basic_example/*.xtdproperty.1.json"
```

**Validate external documents**

```
ajv validate --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdexternaldocument.1.schema.json \
-r "schemas/!(xtdexternaldocument).1.schema.json" \
-d "datasets/basic_example/*.xtdexternaldocument.1.json"
```
**Validate languages**

```
ajv validate --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdlanguage.1.schema.json \
-r "schemas/!(xtdlanguage).1.schema.json" \
-d "datasets/basic_example/*.xtdlanguage.1.json"
```

**Validate 22057 dataset**

```
ajv validate --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdproperty.1.schema.json \
-r "schemas/!(xtdproperty).1.schema.json" \
-d "datasets/22057/*.xtdproperty.1.json"
```