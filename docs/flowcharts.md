# Entity inheritance graph

```mermaid
flowchart TD
    xtdUnitScaleEnum
    xtdUnitBaseEnum
    xtdRoot -- Supertype of --> xtdObject
    xtdRoot -- Supertype of --> xtdInterval
    xtdRoot -- Supertype of --> xtdMultiLanguageText
    xtdRoot -- Supertype of --> xtdText
    xtdRoot -- Supertype of --> xtdRational
    xtdObject -- Supertype of --> xtdConcept
    xtdObject -- Supertype of --> xtdOrderedValue
    xtdObject -- Supertype of --> xtdRelationshipToSubject
    xtdObject -- Supertype of --> xtdValue
    xtdConcept -- Supertype of --> xtdDimension
    xtdConcept -- Supertype of --> xtdValueList
    xtdConcept -- Supertype of --> xtdProperty
    xtdConcept -- Supertype of --> xtdSubject
    xtdConcept -- Supertype of --> xtdUnit
    xtdConcept -- Supertype of --> xtdExternalDocument
    xtdConcept -- Supertype of --> xtdRelationshipType
    xtdConcept -- Supertype of --> xtdRelationshipToProperty
    xtdSubject -- Supertype of --> GroupOfProperties
    GroupOfProperties -- Supertype of --> SetOfProperties
    GroupOfProperties -- Supertype of --> Purpose
    xtdSubject -- Supertype of --> Object
    xtdSubject -- Supertype of --> DataTemplate

    style xtdRoot fill:#bbf
    style xtdObject fill:#bbf
    style xtdConcept fill:#bbf
    style xtdSubject fill:#bbf
    style GroupOfProperties fill:#bbf
    
```

Please note that the entities `GroupOfProperties` and `SetOfProperties` have similar names but different definitions. The same goes for `xtdObject` and `Object`. 

# Entity relation graph

```mermaid
flowchart LR
    xtdSubject -- via Properties --> xtdProperty
    xtdSubject -- via ConnectedSubjects --> xtdRelationshipToSubject
    
    xtdRelationshipToSubject -- via TargetSubjects --> xtdSubject
    xtdRelationshipToSubject -- via ScopeSubjects --> xtdSubject
    xtdRelationshipToSubject -- via RelationshipType --> xtdRelationshipType
    xtdRelationshipType -- Refers to --> xtdRelationshipKindEnum
    xtdProperty -- via PossibleValues --> xtdValueList
    xtdProperty -- via BoundaryValues --> xtdInterval
    xtdInterval -- Refers to --> xtdValueList
    xtdValueList -- Refers to --> xtdOrderedValue
    xtdValueList -- Refers to --> xtdUnit
    xtdUnit -- Refers to --> xtdDimension
    xtdUnit -- Refers to --> xtdUnitScaleEnum
    xtdUnit -- Refers to --> xtdUnitBaseEnum
    xtdUnit -- Refers to --> xtdRational
    xtdDimension -- Refers to --> xtdRational
    xtdOrderedValue -- Refers to --> xtdValue

    style xtdSubject fill:#bbf
```

# Entity instance relations (example)

The following example uses the example relationship types "contains" (inspired by 22057 Figure 10).

```mermaid
flowchart LR
    epd["EN 14351-1 - windows and doors ... (DataTemplate)"] -- via relation #quot;contains#quot; --> a4["Scenario for information module A4 (SetOfProperties)"]
    epd -- via relation #quot;contains#quot; --> sop_level_1["EPD General information (SetOfProperties)"]
    sop_level_1 -- via relation #quot;contains#quot; --> sop_level_2["Product information (SetOfProperties)"]
    sop_level_2 -- via attribute #quot;Properties#quot; --> prop1["Property one (xtdProperty)"]
    sop_level_2 -- via attribute #quot;Properties#quot; --> prop2["Property Two (xtdProperty)"]
    sop_level_2 -- via attribute #quot;Properties#quot; --> prop3["Property Three (xtdProperty)"]

    sop_level_1 -- via relation #quot;contains#quot; --> sop_level_3["Content declaration (SetOfProperties)"]
```
