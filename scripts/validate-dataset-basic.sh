#!/bin/sh

echo ""
echo "Testing xtddictionary"
echo ""

ajv validate --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtddictionary.1.schema.json \
-r "schemas/!(xtddictionary).1.schema.json" \
-d "datasets/**/*.xtddictionary.1.json"

echo ""
echo "Testing xtdproperty"
echo ""

ajv validate --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdproperty.1.schema.json \
-r "schemas/!(xtdproperty).1.schema.json" \
-d "datasets/**/*.xtdproperty.1.json"

echo ""
echo "Testing xtdexternaldocument"
echo ""

ajv validate --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdexternaldocument.1.schema.json \
-r "schemas/!(xtdexternaldocument).1.schema.json" \
-d "datasets/**/*.xtdexternaldocument.1.json"

echo ""
echo "Testing xtdsubject"
echo ""

ajv validate --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdsubject.1.schema.json \
-r "schemas/!(xtdsubject).1.schema.json" \
-d "datasets/**/*.xtdsubject.1.json"

echo ""
echo "Testing xtdrelationshiptype"
echo ""

ajv validate --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdrelationshiptype.1.schema.json \
-r "schemas/!(xtdrelationshiptype).1.schema.json" \
-d "datasets/**/*.xtdrelationshiptype.1.json"