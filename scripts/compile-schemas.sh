#!/bin/sh

ajv compile --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/entityref.1.schema.json \
-r "schemas/!(entityref).1.schema.json"

ajv compile --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/entity-collection.1.schema.json \
-r "schemas/!(entity-collection).1.schema.json"

ajv compile --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdroot.1.schema.json \
-r "schemas/!(xtdroot).1.schema.json"

ajv compile --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdobject.1.schema.json \
-r "schemas/!(xtdobject).1.schema.json"

ajv compile --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdconcept.1.schema.json \
-r "schemas/!(xtdconcept).1.schema.json"

ajv compile --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtddictionary.1.schema.json \
-r "schemas/!(xtddictionary).1.schema.json"

ajv compile --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdproperty.1.schema.json \
-r "schemas/!(xtdproperty).1.schema.json"

ajv compile --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdexternaldocument.1.schema.json \
-r "schemas/!(xtdexternaldocument).1.schema.json"

ajv compile --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdsubject.1.schema.json \
-r "schemas/!(xtdsubject).1.schema.json"

ajv compile --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdunit.1.schema.json \
-r "schemas/!(xtdunit).1.schema.json"

ajv compile --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtduuid.1.schema.json \
-r "schemas/!(xtduuid).1.schema.json"

ajv compile --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdvaluelist.1.schema.json \
-r "schemas/!(xtdvaluelist).1.schema.json"

ajv compile --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdrelationshiptosubject.1.schema.json \
-r "schemas/!(xtdrelationshiptosubject).1.schema.json"

ajv compile --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdrelationshiptype.1.schema.json \
-r "schemas/!(xtdrelationshiptype).1.schema.json"

ajv compile --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdsymbol.1.schema.json \
-r "schemas/!(xtdsymbol).1.schema.json"

ajv compile --all-errors -c ajv-keywords -c ajv-formats --spec=draft2020 \
-s schemas/xtdinterval.1.schema.json \
-r "schemas/!(xtdinterval).1.schema.json"



